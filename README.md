# Tracker-rs for Tracker 3.0
The Rust API is documented here: https://dleggo.gitlab.io/tracker-rs/tracker/index.html.

All the functions that [GIR](https://github.com/gtk-rs/gir) can simply generate are available. Some things haven't been added to tracker-rs yet though:

* The function `foreach` in `NamespaceManager` is not available.
* The function `get_values` in `Resource` is not available.
* The function `update_array_async` in `SparqlConnection` is not available.
* The sparql utility functions are missing.

## Work In Progress
- Write examples to check the basic functionality is working.
- Test flatpak intergration and async functionality.

I do not know enough of Rust, C, or FFI to enable the class and functions above, I will try in the future. Please contribute to enable everything in Tracker 3.x, writing the functions in idiomatic rust, and creating working examples. Also a suite of tests would be awesome! 

## Build
To regenerate the the code from the gir files. Run `./generator.py`

