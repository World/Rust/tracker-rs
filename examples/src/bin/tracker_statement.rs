// A simple demo of tracker::SparqlStatement
use tracker::prelude::*;

fn main() {
    let connection =
        tracker::SparqlConnection::bus_new("org.freedesktop.Tracker3.Miner.Files", None, None)
            .unwrap();

    // Create a reusable statement, where variables prefixed with ~ can be replaces via bind_*
    let statement = connection
        .query_statement(
            "SELECT nie:url(?u) WHERE { ?u a nfo:FileDataObject ; nfo:fileSize ?size . FILTER (?size >= ~size) }",
            None::<&gio::Cancellable>,
        )
        .unwrap()
        .unwrap();

    for size in vec![1000, 2000, 3000, 4000, 5000] {
        // Bind the value of the size variable to size.
        statement.bind_int("size", size);
        let cursor = statement.execute(None::<&gio::Cancellable>).unwrap();

        let mut i = 0;
        // Iterate over the values as usual
        while let Ok(true) = cursor.next(None::<&gio::Cancellable>) {
            println!("Result [{}]: {}", i, cursor.string(0).unwrap().as_str());
            i += 1;
        }

        println!(
            "A total of '{}' results were found for files that are bigger than {}",
            i, size
        );
    }
}
