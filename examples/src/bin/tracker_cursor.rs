// A simple demo of tracker::SparqlConnection
use tracker::prelude::*;

fn main() {
    let connection =
        tracker::SparqlConnection::bus_new("org.freedesktop.Tracker3.Miner.Files", None, None)
            .unwrap();

    let query = "SELECT nie:url(?u) WHERE { ?u a nfo:FileDataObject }";
    let cursor = connection.query(query, None::<&gio::Cancellable>).unwrap();

    let mut i = 0;
    while let Ok(true) = cursor.next(None::<&gio::Cancellable>) {
        println!("Result [{}]: {}", i, cursor.string(0).unwrap().as_str());
        i += 1;
    }

    println!("A total of '{}' results were found", i);
}
