// A simple demo of tracker::Resource

use std::path::Path;
use tracker::prelude::*;

fn main() {
    // The path where the database is stored
    let mut store_path = Path::new(env!("CARGO_MANIFEST_DIR")).to_path_buf();
    store_path.push("store");
    // The path where the ontology (definition of the database schema) is (in a subfolder `ontology`).
    // See the files in the root directory of examples in data/ontology.
    let mut ontology_path = Path::new(env!("CARGO_MANIFEST_DIR")).to_path_buf();
    ontology_path.push("data");
    ontology_path.push("ontology");
    let connection = tracker::SparqlConnection::new(
        tracker::SparqlConnectionFlags::NONE,
        Some(&gio::File::for_path(store_path.clone())),
        Some(&gio::File::for_path(ontology_path.clone())),
        None::<&gio::Cancellable>,
    )
    .unwrap();

    // Create a namespace manager so the resource can resolve our type later on
    let manager = tracker::NamespaceManager::new();
    manager.add_prefix("rustExample", "https://gitlab.com/dleggo/tracker-rs#");

    let resource = tracker::Resource::new(None);
    // Set the type we want to add to the database
    resource.set_uri("rdf:type", "rustExample:ExampleClass");
    // Set data of the type
    resource.set_int("rustExample:example_id", 1);

    println!(
        "Adding data of the ontology {:?} to the Tracker store at {:?} : {}",
        ontology_path,
        store_path,
        resource
            .print_sparql_update(Some(&manager), None)
            .unwrap()
            .as_str()
    );

    connection
        .update(
            resource
                .print_sparql_update(Some(&manager), None)
                .unwrap()
                .as_str(),
            None::<&gio::Cancellable>,
        )
        .unwrap();
}
