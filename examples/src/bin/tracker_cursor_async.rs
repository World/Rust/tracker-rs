// A simple demo of tracker::SparqlConnection
use tracker::prelude::*;

fn main() {
    let context = glib::MainContext::new();
    let main_loop = glib::MainLoop::new(Some(&context), false);
    let m_clone = main_loop.clone();

    context
        .with_thread_default(|| {
            context.spawn_local(async move {
                let connection = tracker::SparqlConnection::bus_new(
                    "org.freedesktop.Tracker3.Miner.Files",
                    None,
                    None,
                )
                .unwrap();

                let cursor = connection
                    .query_future("SELECT nie:url(?u) WHERE { ?u a nfo:FileDataObject }")
                    .await
                    .unwrap();

                let mut i: u32 = 0;
                while let Ok(true) = cursor.next_future().await {
                    println!("Result [{}]: {}", i, cursor.string(0).unwrap().as_str());
                    i += 1;
                }

                println!("A total of '{}' results were found", i);
                m_clone.quit();
            });
        })
        .unwrap();

    main_loop.run();
}
