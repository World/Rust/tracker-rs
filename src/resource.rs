use glib::object::IsA;
use glib::translate::*;
use crate::Resource;

/* The following function is not implemented in crate::auto because of the error message below.

pub trait ResourceExtManual: 'static {
    fn values(&self, property_uri: &str) -> Vec<glib::Value>;
}

impl<O: IsA<Resource>> ResourceExtManual for O {
    #[doc(alias = "tracker_resource_get_values")]
    #[doc(alias = "get_values")]
    pub fn values(&self, property_uri: &str) -> Vec<glib::Value> {
        unsafe {
            FromGlibPtrContainer::from_glib_container(ffi::tracker_resource_get_values(self.to_glib_none().0, property_uri.to_glib_none().0))
        }
    }
}


error[E0277]: the trait bound `Value: GlibPtrDefault` is not satisfied
   --> src/auto/resource.rs:163:13
    |
163 | ...   FromGlibPtrContainer::from_glib_container(ffi::tracker_resource_get_values(self.to_glib_none().0, property_uri.to_glib_none().0))
    |       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ the trait `GlibPtrDefault` is not implemented for `Value`
    |
    = note: required because of the requirements on the impl of `FromGlibContainerAsVec<_, *mut GList>` for `Value`
    = note: required because of the requirements on the impl of `FromGlibContainer<_, *mut GList>` for `Vec<Value>`
    = note: required by `from_glib_container`
*/

