pub use crate::auto::traits::*;

//pub use crate::resource::ResourceExtManual;
pub use crate::sparql_connection::SparqlConnectionExtManual;
pub use crate::sparql_cursor::SparqlCursorExtManual;
