use crate::SparqlCursor;
use glib::translate::*;

pub trait SparqlCursorExtManual {
    #[doc(alias = "tracker_sparql_cursor_get_string")]
    #[doc(alias = "get_string")]
    fn string(&self, column: i32) -> Option<glib::GString>;
}

impl SparqlCursorExtManual for SparqlCursor {
    fn string(&self, column: i32) -> Option<glib::GString> {
        unsafe {
            let ret = from_glib_none(ffi::tracker_sparql_cursor_get_string(
                self.to_glib_none().0,
                column,
                std::ptr::null_mut(),
            ));
            ret
        }
    }
}
