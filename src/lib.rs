macro_rules! assert_initialized_main_thread {
    () => {
        // TODO: Check if tracker is initialized and this is a thread where it's allowed to use the
        // tracker API. Or if tracker does not have such constraints then set
        // `generate_safety_asserts = false` in Gir.toml
    };
}

macro_rules! skip_assert_initialized {
    () => {};
}

mod auto;
pub mod prelude;
pub use auto::*;
pub use ffi;

//mod resource;
mod sparql_connection;
mod sparql_cursor;
