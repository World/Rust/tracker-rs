// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, NotifierEventType};
use glib::translate::*;

glib::wrapper! {
    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct NotifierEvent(Boxed<ffi::TrackerNotifierEvent>);

    match fn {
        copy => |ptr| glib::gobject_ffi::g_boxed_copy(ffi::tracker_notifier_event_get_type(), ptr as *mut _) as *mut ffi::TrackerNotifierEvent,
        free => |ptr| glib::gobject_ffi::g_boxed_free(ffi::tracker_notifier_event_get_type(), ptr as *mut _),
        type_ => || ffi::tracker_notifier_event_get_type(),
    }
}

impl NotifierEvent {
    #[doc(alias = "tracker_notifier_event_get_event_type")]
    #[doc(alias = "get_event_type")]
    pub fn event_type(&mut self) -> NotifierEventType {
        unsafe {
            from_glib(ffi::tracker_notifier_event_get_event_type(
                self.to_glib_none_mut().0,
            ))
        }
    }

    #[doc(alias = "tracker_notifier_event_get_id")]
    #[doc(alias = "get_id")]
    pub fn id(&mut self) -> i64 {
        unsafe { ffi::tracker_notifier_event_get_id(self.to_glib_none_mut().0) }
    }

    #[doc(alias = "tracker_notifier_event_get_urn")]
    #[doc(alias = "get_urn")]
    pub fn urn(&mut self) -> Option<glib::GString> {
        unsafe {
            from_glib_none(ffi::tracker_notifier_event_get_urn(
                self.to_glib_none_mut().0,
            ))
        }
    }
}
