#!/usr/bin/env python3

import sys
import subprocess

if __name__ == "__main__":
    subprocess.run(["git", "submodule", "update", "--init", "--", "gir"], check=True);

    args = sys.argv;
    args[0] = "./gir/generator.py";
    args.append("./gir-files/")

    subprocess.run(args);
