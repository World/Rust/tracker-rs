with import <nixpkgs> {};

pkgs.mkShell {
  buildInputs = [
    pkgs.python3
    pkgs.cargo
    pkgs.rustc
    pkgs.rustfmt
    pkgs.tracker.dev
    pkgs.glib.dev
    pkgs.pkg-config
  ];
}
